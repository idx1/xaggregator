package org.idx.aggregator.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Entity
@Table(name = "AGG_EVENT")
@Document(collection="AggregationEvents")
@Data
public class AggregationEvent {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String uid;
	
	private String uidValue;
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="event_id")
	private List<AggregationAttribute> attributes;
	
	// NOTE: Isko baad me dekhenge
//	private List<SyncAttribute> childAttributes;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="result_id")    
	private AggregationResult result;
	
//	private int linkedAcctountId;
	
	private String failureReason;
	private EVENT_STATE eventState;
	private Date creationDate;
	private Date actionDate;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="profile_id")
	private AggregationProfile aggregationProfile;

//	public String getUidValue() {
//		return uidValue;
//	}
//
//	public void setUidValue(String uidValue) {
//		this.uidValue = uidValue;
//	}
//
//	public List<AggregationAttribute> getAttributes() {
//		return attributes;
//	}
//
//	public void setAttributes(List<AggregationAttribute> attributes) {
//		this.attributes = attributes;
//	}
//
//	public String getUid() {
//		return uid;
//	}
//
//	public void setUid(String uid) {
//		this.uid = uid;
//	}
//
//	public AggregationProfile getAggregationProfile() {
//		return aggregationProfile;
//	}
//
//	public void setAggregationProfile(AggregationProfile aggregationProfile) {
//		this.aggregationProfile = aggregationProfile;
//	}
//
//	public int getId() {
//		return id;
//	}
//
//	public void setId(int id) {
//		this.id = id;
//	}
// 
//	public String getFailureReason() {
//		return failureReason;
//	}
//
//	public void setFailureReason(String failureReason) {
//		this.failureReason = failureReason;
//	}
//
//	public EVENT_STATE getEventState() {
//		return eventState;
//	}
//
//	public void setEventState(EVENT_STATE eventState) {
//		this.eventState = eventState;
//	}
//
//	public Date getCreationDate() {
//		return creationDate;
//	}
//
//	public void setCreationDate(Date creationDate) {
//		this.creationDate = creationDate;
//	}
//
//	public Date getActionDate() {
//		return actionDate;
//	}
//
//	public void setActionDate(Date actionDate) {
//		this.actionDate = actionDate;
//	}
//	
//	
//
}
