package org.idx.aggregator.dao;

 import java.util.List;

import org.idx.aggregator.exception.AggregationServiceException;
import org.idx.aggregator.model.AccessProfile;
import org.idx.aggregator.model.Application;
import org.idx.aggregator.model.ApplicationProfile;
import org.idx.aggregator.model.Entitlement;
import org.idx.application.dob.ApplicationDOB;

public interface ApplicationDAO {
	
	public Entitlement createEntitlement(Entitlement entitlement) throws AggregationServiceException;
	public Entitlement updateEntitlement(Entitlement entitlement) throws AggregationServiceException;
	public void deleteEntitlement(long id) throws AggregationServiceException;
	public Entitlement findEntitlement(long id) throws AggregationServiceException;
	public Entitlement findEntitlement(String name) throws AggregationServiceException;
	public void enableEntitlement(long id) throws AggregationServiceException;
	public void enableEntitlement(String name) throws AggregationServiceException;
	public void disableEntitlement(long id) throws AggregationServiceException;
	public void disableEntitlement(String name) throws AggregationServiceException;
	public List<Entitlement> findEntitlementForApplication(String applicationName) throws AggregationServiceException;
	public List<Entitlement> findEntitlementForApplication(long applicationId) throws AggregationServiceException;

	public ApplicationProfile createApplicationProfile(ApplicationProfile applicationProfile) throws AggregationServiceException;
	public ApplicationProfile updateApplicationProfile(ApplicationProfile applicationProfile) throws AggregationServiceException;
	public void deleteApplicationProfile(long id) throws AggregationServiceException;
	public ApplicationProfile findApplicationProfile(long id) throws AggregationServiceException;
	public ApplicationProfile findApplicationProfile(String name) throws AggregationServiceException;
	public void enableApplicationProfile(long id) throws AggregationServiceException;
	public void enableApplicationProfile(String name) throws AggregationServiceException;
	public void disableApplicationProfile(long id) throws AggregationServiceException;
	public void disableApplicationProfile(String name) throws AggregationServiceException;

	public List<ApplicationProfile> findApplicationProfileForApplication(String applicationName) throws AggregationServiceException;
	public List<ApplicationProfile> findApplicationProfileForApplication(long applicationId) throws AggregationServiceException;


	public AccessProfile createAccessProfile(AccessProfile accessProfile) throws AggregationServiceException;
	public AccessProfile updateAccessProfile(AccessProfile accessProfile) throws AggregationServiceException;
	public void deleteAccessProfile(long id) throws AggregationServiceException;
	public AccessProfile findAccessProfile(long id) throws AggregationServiceException;
	public AccessProfile findAccessProfile(String name) throws AggregationServiceException;
	public void enableAccessProfile(long id) throws AggregationServiceException;
	public void enableAccessProfile(String name) throws AggregationServiceException;
	public void disableAccessProfile(long id) throws AggregationServiceException;
	public void disableAccessProfile(String name) throws AggregationServiceException;
	public Application findApplication(String name) throws AggregationServiceException;
	public List<Application> getApplicationList() throws AggregationServiceException;


 
}
