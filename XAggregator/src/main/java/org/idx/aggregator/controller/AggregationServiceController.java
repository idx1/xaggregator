package org.idx.aggregator.controller;

import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.beanutils.BeanUtils;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.idx.aggregator.exception.AggregationServiceException;
import org.idx.aggregator.model.Application;
import org.idx.aggregator.model.ApplicationType;
import org.idx.aggregator.model.Entitlement;
import org.idx.aggregator.service.AggregationService;
import org.idx.application.dob.AggregationProfileDOB;
import org.idx.application.dob.ApplicationDOB;
import org.idx.application.dob.ApplicationTypeDOB;
import org.idx.application.dob.ConfigurationParamsDOB;
import org.idx.application.dob.ConfigurationParamsDOB.CONFIG_PARAM_TYPE;
import org.idx.common.rest.ServerResponse;
import org.idx.common.rest.ServerResponseCode;
import org.idx.application.dob.ConfiguratorDOB;
import org.idx.application.dob.EntitlementDOB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * 
 * TODO: 
 * 
 * 1. Change Object type conversion to generic method. Check ApplicationServiceController mapList and mapObject method. 
 * Reason: Avoid boilerplate code
 * 
 * 
 * 
 * 
 * 
 * 
 * @author Anurag Singh
 *
 */


@RestController
@RequestMapping("/aggregator/")
public class AggregationServiceController {
	
	@Autowired
	private AggregationService aggregationService;
	
	private Mapper mapper = new DozerBeanMapper();

	
	@PostMapping("/createApplication")
	public ResponseEntity<Object> createApplication(@RequestBody ApplicationDOB appDOB) throws AggregationServiceException {
		
		Application application = getApplication(appDOB);
		
		System.out.println("appDOB : "+ appDOB);
		System.out.println("application : " + application);
		
		Application savedApp = null;
		
		savedApp = aggregationService.createApplication(application );
			
 		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedApp.getId()).toUri();

		return ResponseEntity.created(location).build();

	}

	
	@PostMapping("/createApplicationType")
	public ResponseEntity<Object> createApplicationType(@RequestBody ApplicationTypeDOB appTypeDOB) throws AggregationServiceException {
		
		ApplicationType appType = getApplicationType(appTypeDOB);
		
		System.out.println("appTypeDOB : "+ appTypeDOB);
		System.out.println("appType : " + appType);
		
		ApplicationType savedAppType = aggregationService.createApplicationType(appType );
			
 		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedAppType.getId()).toUri();

		return ResponseEntity.created(location).build();

	}


	


	@GetMapping("/getAppTypeList")
	public ApplicationTypeDOB[] getApplicationTypeList() throws AggregationServiceException {
		
		
		System.out.println("aaya re app type lene : getAppTypeList : ");

		
		List<ApplicationType> appTypeList = aggregationService.getApplicationTypeList();
		
		System.out.println("ye mila : getAppTypeList : " + appTypeList);

		ApplicationTypeDOB[] appTypeDOBList = new ApplicationTypeDOB[appTypeList.size()];
		
		int i=0;
		for(ApplicationType appType:appTypeList) {
			ApplicationTypeDOB appTypleDOB = getApplicationDOB(appType);
			appTypeDOBList[i] = appTypleDOB;
			i++;
		}
		
		System.out.println("fir ye mila : getAppTypeList : " + appTypeDOBList);

		return appTypeDOBList;
	}
	
	private ApplicationTypeDOB getApplicationDOB(ApplicationType appType) {

		ApplicationTypeDOB appTypeDOB =  
			    mapper.map(appType, ApplicationTypeDOB.class);
		return appTypeDOB;
	}

	private ApplicationType getApplicationType(ApplicationTypeDOB appTypeDOB) {
		ApplicationType appType =  
			    mapper.map(appTypeDOB, ApplicationType.class);
		return appType;
	}


	private Application getApplication(ApplicationDOB appDOB) {
		Application app =  
		    mapper.map(appDOB, Application.class);

		return app;
	}

	private ApplicationDOB getApplicationDOB(Application app) {
		ApplicationDOB appDOB =  
		    mapper.map(app, ApplicationDOB.class);

		return appDOB;
	}













	public static void main(String args[]) {
		
		ApplicationDOB appdob = new ApplicationDOB();
		
		ConfiguratorDOB configuratordob = new ConfiguratorDOB();
		
		List<ConfigurationParamsDOB> configParams = new ArrayList<ConfigurationParamsDOB>();
		
		ConfigurationParamsDOB configParam1 = new ConfigurationParamsDOB();
		configParam1.setName("param1");
		configParam1.setValue("Val1");
		CONFIG_PARAM_TYPE type = CONFIG_PARAM_TYPE.STRING;
		configParam1.setType(type );

		ConfigurationParamsDOB configParam2 = new ConfigurationParamsDOB();
		configParam2.setName("param1");
		configParam2.setValue("Val1");
		configParam2.setType(type );

		configParams.add(configParam1);
		configParams.add(configParam2);

		configuratordob.setConfigParams(configParams );
		
		AggregationProfileDOB profile = new AggregationProfileDOB();
		profile.setName("Test App Profile");
 

		
		
		appdob.setName("Test App");
		configuratordob.setName("Test App Configurator");
		appdob.setConfigurator(configuratordob );
		appdob.setProfile(profile);

		System.out.println(appdob);
		
		

		
		Mapper mapper = new DozerBeanMapper();
		Application app =  
		    mapper.map(appdob, Application.class);

//		try {
//			BeanUtils.copyProperties(app, appdob);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		
		System.out.println(app);
		System.out.println(appdob);
		System.out.println(app.getName());
		System.out.println(app.getConfigurator().getName());
	}
	
	
	static<FROM, TO>  Object mapObject(Object obj, Class<TO> objClass){
		
		Mapper mapper = new DozerBeanMapper();

		Object retObj = mapper.map(obj, objClass);
		
		return retObj;
		
	}

	<FROM, TO> List<TO> mapList(List<FROM> fromList, final Class<TO> toClass) {
	    return fromList
	            .stream()
	            .map(from -> this.mapper.map(from, toClass))
	            .collect(Collectors.toList());
	}
	


}
