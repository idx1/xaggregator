package org.idx.aggregator.utils;

public class IDXAggregatorContants {

	public static final String FLATFILE = "FlatFile";
	public static final String ACTIVE_DIRECTORY = "ad";
	public static final String LDAP = "ldap";
	public static final String DATABASE = "db";
	public static final String GOOGLE_APPS = "google_apps";
	public static final String OFFICE_360 = "office360";
	public static final String WORKDAY = "workday";

}
