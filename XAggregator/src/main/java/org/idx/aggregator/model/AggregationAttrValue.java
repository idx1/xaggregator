package org.idx.aggregator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "AGG_ATTR_VALUE")
@Data
public class AggregationAttrValue {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String value;
	
	@ManyToOne
    @JoinColumn(name="attr_id")
	private AggregationAttribute aggregationAttr;

 
}
