package org.idx.aggregator.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "APP_PROFILE")
@Data
public class ApplicationProfile {

	@Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    private long id;
    
	@Column(unique = true)
    private String name;

    private String displayName;

    private String description;
    
	@ManyToOne(cascade=CascadeType.MERGE)
	@JoinColumn(name="application_id")
    private Application application;

    @OneToMany(cascade = { 
            CascadeType.MERGE
        })
        @JoinTable(name = "app_profile_ent",
            joinColumns = @JoinColumn(name = "app_profile_id"),inverseJoinColumns = @JoinColumn( name="ent_id",unique=true)
        )
    private List<Entitlement> entitlements;

    @OneToMany(cascade = { 
            CascadeType.MERGE
        })
        @JoinTable(name = "app_profile_role",
            joinColumns = @JoinColumn(name = "app_profile_id"), inverseJoinColumns = @JoinColumn( name="app_role_id")
        )    private List<ApplicationRole> appRoles;

    private boolean valid;
	
    private String createdBy;

    private String updatedBy;

    private Date createdDate;
    
    private Date updatedDate;

}
