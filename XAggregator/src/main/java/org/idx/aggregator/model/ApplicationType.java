package org.idx.aggregator.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "APPLICATION_TYPE")
@Data
public class ApplicationType {

	@Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    private long id;
    
	@Column(unique = true)
    private String name;
    
    private String displayName;

    private String description;
    
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="param_id")
    private List<AppTypeParams> appTypeParams;

}
