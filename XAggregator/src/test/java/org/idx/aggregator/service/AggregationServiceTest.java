package org.idx.aggregator.service;

import java.util.ArrayList;
import java.util.List;

import org.idx.aggregator.exception.AggregationServiceException;
import org.idx.aggregator.model.ATTR_TYPE;
import org.idx.aggregator.model.AggregationAttrValue;
import org.idx.aggregator.model.AggregationAttribute;
import org.idx.aggregator.model.AggregationEvent;
import org.idx.aggregator.model.AggregationProfile;
import org.idx.aggregator.model.Application;
import org.idx.aggregator.model.ConfigurationParams;
import org.idx.aggregator.model.ConfigurationParams.CONFIG_PARAM_TYPE;
import org.idx.aggregator.model.Configurator;
import org.idx.aggregator.model.Entitlement;
import org.idx.aggregator.model.MATCHING_ACTION;
import org.idx.aggregator.model.MATCHING_CONDITION;
import org.idx.aggregator.model.MatchingAction;
import org.idx.aggregator.model.MatchingRule;
import org.idx.aggregator.model.ProfileAttribute;
import org.idx.aggregator.utils.IDXAggregatorContants;
import org.idx.core.engine.IDXContext;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class AggregationServiceTest {

    @Autowired
    AggregationService aggregationService;

    @Autowired
    ApplicationService appService;
    
    
	@Test
	@Transactional
	@Rollback(false)
	public void testfindEntitlementforApp() throws AggregationServiceException {
		List<Entitlement> ents = appService.findEntitlementForApplication("Flat File 1");
		
		System.out.println("ents : "+ ents);
	}

//	@Test
//	@Transactional
//	@Rollback(false)
//	public void testCreateAggregationProfile() {
    
    public AggregationProfile getProfile() {
		
		AggregationProfile profile = new AggregationProfile();
		
		profile.setName("Workday Onboarding");
		
		List<ProfileAttribute> profileAttrs = new ArrayList<ProfileAttribute>();
		
		
		ProfileAttribute attr1 = new ProfileAttribute();
		
		attr1.setName("First Name");
		attr1.setTargetAttrName("First Name");
		attr1.setType(ProfileAttribute.PR_ATTR_TYPE.STRING);
		
		ProfileAttribute attr2 = new ProfileAttribute();
		
		attr2.setName("Middle Name");
		attr2.setTargetAttrName("Middle Name");
		attr2.setType(ProfileAttribute.PR_ATTR_TYPE.STRING);
		
		ProfileAttribute attr3 = new ProfileAttribute();
		
		attr3.setName("Last Name");
		attr3.setTargetAttrName("Last Name");
		attr3.setType(ProfileAttribute.PR_ATTR_TYPE.STRING);
		
		ProfileAttribute attr4 = new ProfileAttribute();
		
		attr4.setName("Email");
		attr4.setTargetAttrName("Email");
		attr4.setType(ProfileAttribute.PR_ATTR_TYPE.STRING);
		
		profileAttrs.add(attr1);
		profileAttrs.add(attr2);
		profileAttrs.add(attr3);
		profileAttrs.add(attr4);
		
		profile.setProfileAttr(profileAttrs);
		
		
		
		List<MatchingAction> matchingActionList = new ArrayList<MatchingAction>();
		MatchingAction matchingActionCreate= new MatchingAction();
		matchingActionCreate.setAggAction(MATCHING_ACTION.CREATE);
		
		MatchingAction matchingActionIgnore=new MatchingAction();
		matchingActionIgnore.setAggAction(MATCHING_ACTION.IGNORE);
		
		MatchingAction matchingActionDelete=new MatchingAction();
		matchingActionDelete.setAggAction(MATCHING_ACTION.DELETE);

		MatchingAction matchingActionUpdate=new MatchingAction();
		matchingActionUpdate.setAggAction(MATCHING_ACTION.UPDATE);

		
		matchingActionList.add(matchingActionCreate);
		matchingActionList.add(matchingActionIgnore);
		matchingActionList.add(matchingActionDelete);
		matchingActionList.add(matchingActionUpdate);
		
		profile.setMatchingActions(matchingActionList);
		
		MatchingRule matchingRule = new MatchingRule();
		String idxAttibute="Employee Number";


		MATCHING_CONDITION matchingCondition = MATCHING_CONDITION.EQUALS;
		String tgtAttribute = "empId";

		matchingRule.setIdxAttribute(idxAttibute);
		matchingRule.setTgtAttribute(tgtAttribute);
		matchingRule.setMatchingCondition(matchingCondition);
		
		profile.setMatchingRule(matchingRule );
		
		return profile;
//		aggregationService.createAggregationProfile(profile );
	}
	
	
//	@Test
//	@Transactional
//	@Rollback(false)
//	public void testCreateConfigurator() {
	
	public Configurator getConfigurator() {
		Configurator flatfileConfigurator = new Configurator();
		
		flatfileConfigurator.setName("Workday OnBoarding Configurator");
		flatfileConfigurator.setDisplayName("Workday onboarding configurator");
		
		flatfileConfigurator.setType(IDXAggregatorContants.FLATFILE);
		flatfileConfigurator.setProfileName("Workday Onboarding");
		
		List<ConfigurationParams> configParams = new ArrayList<ConfigurationParams>();
		
		ConfigurationParams param1 = new ConfigurationParams();
		param1.setName("flatFileDirectory");
		param1.setValue("E:/work/flatfile/WD_Onboard.csv");
		param1.setType(CONFIG_PARAM_TYPE.STRING);
		
		ConfigurationParams param2 = new ConfigurationParams();
		param2.setName("accountIDColumn");
		param2.setValue("Employee ID");
		param2.setType(CONFIG_PARAM_TYPE.STRING);
		
		ConfigurationParams param3 = new ConfigurationParams();
		param3.setName("flatFileBundleDirectory");
		param3.setValue("E:/work/flatfile");
		param3.setType(CONFIG_PARAM_TYPE.STRING);
		
		ConfigurationParams param4 = new ConfigurationParams();
		param4.setName("flatFileBundlePath");
		param4.setValue("org.connid.bundles.flatfile-1.2.jar");
		param4.setType(CONFIG_PARAM_TYPE.STRING);
		
		ConfigurationParams param5 = new ConfigurationParams();
		param5.setName("bundleVersion");
		param5.setValue("1.2");
		param5.setType(CONFIG_PARAM_TYPE.STRING);
		
		ConfigurationParams param6 = new ConfigurationParams();
		param6.setName("connectorName");
		param6.setValue("org.identityconnectors.flatfile.FlatFileConnector");
		param6.setType(CONFIG_PARAM_TYPE.STRING);
		
		ConfigurationParams param7 = new ConfigurationParams();
		param7.setName("bundleName");
		param7.setValue("org.connid.bundles.flatfile");
		param7.setType(CONFIG_PARAM_TYPE.STRING);

		ConfigurationParams param8 = new ConfigurationParams();
		param8.setName("fileEncoding");
		param8.setValue("UTF-8");
		param8.setType(CONFIG_PARAM_TYPE.STRING);

		ConfigurationParams param9 = new ConfigurationParams();
		param9.setName("FieldDelimiter");
		param9.setValue(",");
		param9.setType(CONFIG_PARAM_TYPE.STRING);

		configParams.add(param1);
		configParams.add(param2);
		configParams.add(param3);
		configParams.add(param4);
		configParams.add(param5);
		configParams.add(param6);
		configParams.add(param7);
		configParams.add(param8);
		configParams.add(param9);
		
		flatfileConfigurator.setConfigParams(configParams );
		
		return flatfileConfigurator;

//		aggregationService.creatConfigurator(flatfileConfigurator);
	}
	
//	@Test
//	@Transactional
//	@Rollback(false)
	public void testCreateApplication() {
		
		Application application = new Application();

		application.setName("Workday Onboarding Test");
		application.setDescription("Workday onBoarding application for on Boarding users in vfID");
		
		application.setTrusted(true);
		
		Configurator configurator = getConfigurator();
		application.setConfigurator(configurator);
		
		AggregationProfile profile = getProfile();
		application.setProfile(profile);
		
		try {
			aggregationService.createApplication(application);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
	}
	
//	@Test
//	@Transactional
//	@Rollback(false)
	public void testPerformReconciliation() throws AggregationServiceException {
		
		
		Application app = aggregationService.getApplication("Flat File 1");

//		Configurator configurator = getConfigurator();
		IDXContext context = null;
		try {
			
			long start = System.currentTimeMillis();
			
			aggregationService.performReconciliation(context, app);
			
			long end = System.currentTimeMillis();

			long time = (end - start);
			
			System.out.println("Time for recon run : " + (end - start) + " ms");
			
			Thread.sleep(120000);
			
			System.out.println("Ho Haya ke : " + time + " ms");

		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
//	@Test
//	@Transactional
//	@Rollback(false)
	public void testCreateAggregationEvent() throws AggregationServiceException {
		
		AggregationEvent event = null;
		
		long startTime = System.currentTimeMillis();
		
		for(int i=0; i < 5;i++) {
		
		int id=	aggregationService.getNextSequence("AggSeq");
			
		event = new AggregationEvent();
		event.setId(id);
		event.setUid(id+"");
		
		AggregationProfile profile = new AggregationProfile();
		profile.setName("abc");
		event.setAggregationProfile(profile);

		List<AggregationAttribute> attributes = new ArrayList<AggregationAttribute>();
		
		AggregationAttribute att1 = new AggregationAttribute();
		
		att1.setName("First Name");
		att1.setType(ATTR_TYPE.STRING );
		
		AggregationAttrValue attVal1 = new AggregationAttrValue();
		attVal1.setValue("Ramu" + i);
		
		List<AggregationAttrValue> value1 = new ArrayList<AggregationAttrValue>();
		value1.add(attVal1);
		att1.setValue(value1 );
		
		AggregationAttribute att2 = new AggregationAttribute();
		
		att2.setName("Last Name");
		att2.setType(ATTR_TYPE.STRING );
		
		AggregationAttrValue attVal2 = new AggregationAttrValue();
		attVal2.setValue("Kaka" + i);
		
		List<AggregationAttrValue> value2 = new ArrayList<AggregationAttrValue>();
		value2.add(attVal2);
		
		att2.setValue(value2);

		AggregationAttribute att3 = new AggregationAttribute();
		att3.setName("Email");
		att3.setType(ATTR_TYPE.STRING );
		
		AggregationAttrValue attVal3 = new AggregationAttrValue();
		attVal3.setValue("ramu.kaka"+i+"@idx.com");
		
		List<AggregationAttrValue> value3 = new ArrayList<AggregationAttrValue>();
		value3.add(attVal3);
		
		att3.setValue(value3);

		
		attributes.add(att1);
		attributes.add(att2);
		attributes.add(att3);
		
		event.setAttributes(attributes);
		
		event = aggregationService.createAggregationEvent(event , "Profile name de na re baba" );

		//System.out.println("event : "+ event);
		//System.out.println("event id : "+ event.getId());
		
		}
		
		long endTime = System.currentTimeMillis();
		
		
		System.out.println("Total Time : "+ (endTime - startTime) + " ms");

	}
	
	
}
