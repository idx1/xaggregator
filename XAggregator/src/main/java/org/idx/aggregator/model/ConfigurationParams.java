package org.idx.aggregator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
 
@Entity
@Table(name = "AGG_CONFIG_PARAMS")
@Data
public class ConfigurationParams {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(nullable=false)
	private String name;

	private String description;

	@Column(nullable=false)
	private CONFIG_PARAM_TYPE type;

	@Column(nullable=false)
	private String value;	
	
	
//	@ManyToOne
//    @JoinColumn(name="config_id")
//	private Configurator configurator;
//
	
	public enum CONFIG_PARAM_TYPE{
		STRING,NUMBER,BOOLEAN,DATE,SECRET;
	}	
}
