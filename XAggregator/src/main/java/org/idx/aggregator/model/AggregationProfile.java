package org.idx.aggregator.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.idx.application.dob.MatchingActionDOB;

import lombok.Data;

 
@Entity
@Table(name = "AGG_PROFILE")
@Data
public class AggregationProfile {
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(unique=true)
	private String name;

	private String tableName;
	
	private AGGREGATION_TYPE aggregationType;
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="profile_id")
	private List<ProfileAttribute> profileAttr;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="rule_id")
	private MatchingRule matchingRule;

	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="action_id")
	private List<MatchingAction> matchingActions;



}
