package org.idx.aggregator.dao;

import java.util.List;

import org.idx.aggregator.model.AggregationEvent;
import org.idx.aggregator.model.EVENT_STATE;

public interface AggregationNOSQLDAO {
	
    public int getNextSequence(String seqName);

	public AggregationEvent getAggregationEvent(long id);
	
	public AggregationEvent getAllEventsForProfile(String profileName);

	public AggregationEvent getAllEventForAggregation(String aggregationId);

	public AggregationEvent createAggregationEvent(AggregationEvent event) ;

	public void updateEventStatus(AggregationEvent event, EVENT_STATE eventState);

}
