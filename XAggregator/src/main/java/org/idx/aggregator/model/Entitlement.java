package org.idx.aggregator.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "ENTITLEMENT")
@Data
public class Entitlement {
	
	@Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    private long id;
    
	@Column(unique = true)
    private String name;
    
    private String displayName;

    private String value;

    private boolean valid;

    private String createdBy;

    private String updatedBy;

    private Date createdDate;
    
    private Date updatedDate;
    
	@ManyToOne(cascade=CascadeType.MERGE)
	@JoinColumn(name="application_id", nullable=false)
    private Application application;

    
    
    


}
