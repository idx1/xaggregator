package org.idx.aggregator.jms;

import org.springframework.util.ErrorHandler;

public class DefaultErrorHandler implements ErrorHandler {


    public void handleError(Throwable t) {
        System.out.println("-----------------> spring jms custom error handling example");
        System.out.println("################  " + t.getCause().getMessage());
    }
}

