package org.idx.aggregator.engine;

import java.io.File;
import java.net.URL;
import java.util.List;

import javax.persistence.Transient;

import org.identityconnectors.common.IOUtil;
import org.identityconnectors.flatfile.FlatFileConfiguration;
import org.identityconnectors.flatfile.FlatFileConnector;
import org.identityconnectors.framework.api.APIConfiguration;
import org.identityconnectors.framework.api.ConfigurationProperties;
import org.identityconnectors.framework.api.ConfigurationProperty;
import org.identityconnectors.framework.api.ConnectorFacade;
import org.identityconnectors.framework.api.ConnectorFacadeFactory;
import org.identityconnectors.framework.api.ConnectorInfo;
import org.identityconnectors.framework.api.ConnectorInfoManager;
import org.identityconnectors.framework.api.ConnectorInfoManagerFactory;
import org.identityconnectors.framework.api.ConnectorKey;
import org.identityconnectors.framework.common.objects.ConnectorObject;
import org.identityconnectors.framework.common.objects.ObjectClass;
import org.identityconnectors.framework.common.objects.OperationOptions;
import org.identityconnectors.framework.common.objects.ResultsHandler;
import org.identityconnectors.framework.common.objects.SearchResult;
import org.identityconnectors.framework.common.objects.filter.Filter;
import org.identityconnectors.framework.common.objects.filter.FilterVisitor;
import org.idx.aggregator.common.IDXMessages;
import org.idx.aggregator.common.MessageUtils;
import org.idx.aggregator.exception.AggregationServiceException;
import org.idx.aggregator.exception.IDXAggregationException;
import org.idx.aggregator.exception.InvalidProfileException;
import org.idx.aggregator.logging.IDXLogger;
import org.idx.aggregator.model.AggregationEvent;
import org.idx.aggregator.model.AggregationProfile;
import org.idx.aggregator.model.ConfigurationParams;
import org.idx.aggregator.service.AggregationService;
import org.idx.aggregator.utils.ConnectorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FlatFileConfigurator implements ResultsHandler{
	
	@Transient
	private static IDXLogger logger = new IDXLogger(FlatFileConfigurator.class);
	
	AggregationProfile profile = null;
	
	@Autowired
	AggregationService aggregationService;

	public void performReconciliation(List<ConfigurationParams> params, AggregationProfile profile) throws InvalidProfileException, IDXAggregationException {
 
		if ((profile ==null)) {
			logger.error(IDXMessages.IDX001_INVALID_PROFILE);
			throw new InvalidProfileException(
					MessageUtils.getExceptionMessage(IDXMessages.IDX001_INVALID_PROFILE));
		}
		
		this.profile = profile;

		
		String FILENAME = null;// "E:\\work\\flatfile\\test.csv";
		String ACCOUNTID = null;// "accountid";
		String flatFileBundleDirectory = null;
		String flatFileBundlePath = null;
		String bundleName = null;
		String bundleVersion = null;
		String connectorName = null;
		String fileEncoding = null;
		char fieldDelimiter = ',';
		
		System.out.println("params : " + params);

		for(ConfigurationParams param: params) {
			String name = param.getName();
			
			if(name.equals(IDXMessages.FLATFILE_PATH))
				FILENAME = param.getValue();
			else if(name.equals(IDXMessages.FLATFILE_ACCOUNTID))
				ACCOUNTID = param.getValue();
			else if(name.equals(IDXMessages.FLATFILE_BUNDLE_DIR))
				flatFileBundleDirectory = param.getValue();
			else if(name.equals(IDXMessages.FLATFILE_BUNDLE_PATH))
				flatFileBundlePath = param.getValue();
			else if(name.equals(IDXMessages.FLATFILE_BUNDLE_NAME))
				bundleName = param.getValue();
			else if(name.equals(IDXMessages.FLATFILE_BUNDLE_VERSION))
				bundleVersion = param.getValue();
			else if(name.equals(IDXMessages.FLATFILE_CONNECTOR_NAME))
				connectorName = param.getValue();
			else if(name.equals(IDXMessages.FLATFILE_FILE_ENCODING))
				fileEncoding = param.getValue();
			else if(name.equals(IDXMessages.FLATFILE_FIELD_DELIM))
				fieldDelimiter = param.getValue().toCharArray()[0];
		}
			
		System.out.println("FILENAME : " + FILENAME);
		
		FlatFileConfiguration config = new FlatFileConfiguration();
		config.setFile(new File(FILENAME));
		config.setUniqueAttributeName(ACCOUNTID);
		config.validate();
		// create a new connector..
		FlatFileConnector cnt = new FlatFileConnector();
		cnt.init(config);

		ConnectorInfoManagerFactory fact = ConnectorInfoManagerFactory.getInstance();

		File bundleDirectory = new File(flatFileBundleDirectory);

		URL url;
		try {
			url = IOUtil.makeURL(bundleDirectory, "/" + flatFileBundlePath);
			logger.debug("bundleName --> " + bundleName);
			logger.debug("bundleVersion --> " + bundleVersion);
			logger.debug("connectorName --> " + connectorName);

			ConnectorInfoManager manager = fact.getLocalManager(url);
			ConnectorKey key = new ConnectorKey(bundleName, bundleVersion,
					connectorName);

			logger.debug("url --> " + url);
			logger.debug("key --> " + key);
			logger.debug("manager --> " + manager);
			logger.debug("ConnectorInfos --> " + manager.getConnectorInfos());

			List<ConnectorInfo> l = manager.getConnectorInfos();

			for (ConnectorInfo ll : l) {
				logger.debug("BundleName $$$ "
						+ ll.getConnectorKey().getBundleName());
				logger.debug("BundleVersion $$$ "
						+ ll.getConnectorKey().getBundleVersion());
				logger.debug("ConnectorName $$$ "
						+ ll.getConnectorKey().getConnectorName());
			}
			ConnectorInfo info = manager.findConnectorInfo(key);

			logger.debug("ConnectorInfo $$$ " + info);

			// From the ConnectorInfo object, create the default
			// APIConfiguration.
			APIConfiguration apiConfig = info.createDefaultAPIConfiguration();

			// From the default APIConfiguration, retrieve the
			// ConfigurationProperties.
			ConfigurationProperties properties = apiConfig
					.getConfigurationProperties();

			ConnectorFacadeFactory factory = ConnectorFacadeFactory
					.getInstance();

			// Print out what the properties are (not necessary).
			List<String> propertyNames = properties.getPropertyNames();
			for (String propName : propertyNames) {
				ConfigurationProperty prop = properties.getProperty(propName);
				logger.debug("Property Name: " + prop.getName()
						+ "\tProperty Type: " + prop.getType());
			}

			properties.setPropertyValue("fieldDelimiter", fieldDelimiter);
			properties.setPropertyValue("file", new File(FILENAME));
			properties.setPropertyValue("uniqueAttributeName", ACCOUNTID);
			properties.setPropertyValue("encoding", fileEncoding);

			logger.debug("fieldDelimiter : " + fieldDelimiter);
			logger.debug("FILENAME : " + FILENAME);
			logger.debug("ACCOUNTID : " + ACCOUNTID);
			logger.debug("fileEncoding : " + fileEncoding);

			// Use the ConnectorFacadeFactory's newInstance() method to get a
			// new connector.
			ConnectorFacade conn = ConnectorFacadeFactory.getInstance()
					.newInstance(apiConfig);

			// Make sure you have set up the Configuration properly.
			conn.validate();

			logger.debug("Validated now");

			// TODO Need to think on Filter
			Filter filter = new NoFilter();
			OperationOptions options = null;
			// ResultsHandler handler = new MyResultsHandler();
			ResultsHandler handler = this;
			SearchResult result = conn.search(ObjectClass.ACCOUNT, filter, handler, options);
			
			System.out.print("result : " + result);
//			System.out.print("result getRemainingPagedResults : " + result.getRemainingPagedResults());
//			System.out.print("result getPagedResultsCookie : " + result.getPagedResultsCookie());
//			System.out.print("result isAllResultsReturned : " + result.isAllResultsReturned());

		} catch (Exception e) {
			e.printStackTrace();
			throw new IDXAggregationException(e.getMessage());
		}

	}
		
	// TODO: This can be used for validation framework
		static class NoFilter implements Filter {
			public boolean accept(ConnectorObject obj) {
				return true;
			}

			public <R, P> R accept(FilterVisitor<R, P> v, P p) {
				// TODO Auto-generated method stub
				return null;
			}
		}


		public boolean handle(ConnectorObject connObject) {
			logger.debug("my connector object : " + connObject);
			logger.debug("my connector object : " + connObject.getUid());
			logger.debug("my connector object : " + connObject.getAttributes());

			String profileName = profile.getName();
			
			AggregationEvent event = ConnectorUtils.getAggregationEvent(connObject, profileName );
 			
			int id=-1;
			try {
				id = aggregationService.getNextSequence("AggSeq");
				event.setId(id);
				
				aggregationService.createAggregationEvent(event, profileName);

			} catch (AggregationServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return true;
		}


 

}
