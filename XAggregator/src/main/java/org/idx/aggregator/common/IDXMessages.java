package org.idx.aggregator.common;

public class IDXMessages {

	public static final String IDX001_INVALID_PROFILE = "IDX001_INVALID_PROFILE";
	
	public static final String FLATFILE_DIR = "flatFileDirectory";

	public static final String FLATFILE_PATH = "fileName";

	public static final String FLATFILE_ACCOUNTID_COLUMN = "accountIDColumn";

	public static final String FLATFILE_ACCOUNTID = "ACCOUNTID";

	public static final String FLATFILE_BUNDLE_DIR = "Bundle Directory";

	public static final String FLATFILE_BUNDLE_PATH = "Bundle Path";

	public static final String FLATFILE_BUNDLE_VERSION = "Bundle Version";

	public static final String FLATFILE_CONNECTOR_NAME = "Connector Name";

	public static final String FLATFILE_BUNDLE_NAME = "Bundle Name";

	public static final String FLATFILE_FILE_ENCODING = "File Encoding";

	public static final String FLATFILE_FIELD_DELIM = "File Delimiter";

}
