package org.idx.aggregator.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

import javax.persistence.Id;

@Entity
@Table(name = "AGG_CONFIGURATOR")
@Data
public class Configurator {

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    private String name;
    
    private String displayName;

    private String profileName;

    private String version;
    
    private String className;

    private String type;

	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="config_id")
    private List<ConfigurationParams> configParams;

//	public long getId() {
//		return id;
//	}
//
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public String getDisplayName() {
//		return displayName;
//	}
//
//	public void setDisplayName(String displayName) {
//		this.displayName = displayName;
//	}
//
//	public String getVersion() {
//		return version;
//	}
//
//	public void setVersion(String version) {
//		this.version = version;
//	}
//
//	public String getClassName() {
//		return className;
//	}
//
//	public void setClassName(String className) {
//		this.className = className;
//	}
//
//	public List<ConfigurationParams> getConfigParams() {
//		return configParams;
//	}
//
//	public void setConfigParams(List<ConfigurationParams> configParams) {
//		this.configParams = configParams;
//	}
//
//	public String getType() {
//		return type;
//	}
//
//	public void setType(String type) {
//		this.type = type;
//	}
//
//	public String getProfileName() {
//		return profileName;
//	}
//
//	public void setProfileName(String profileName) {
//		this.profileName=profileName;		
//	}
//
//     
    

    
}
