package org.idx.aggregator.exception;

public class AggregationServiceExceptionOLD extends Exception {

	public AggregationServiceExceptionOLD() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AggregationServiceExceptionOLD(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public AggregationServiceExceptionOLD(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public AggregationServiceExceptionOLD(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public AggregationServiceExceptionOLD(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
