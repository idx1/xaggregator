package org.idx.aggregator.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.idx.application.dob.MATCHING_ACTION_CONDITION;

import lombok.Data;

@Entity
@Table(name = "AGG_MATCHING_RULE")
@Data
public class MatchingRule {
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String idxAttribute;
	
	private String tgtAttribute;
	
//	@OneToMany
//    @JoinColumn(name="action_id")
//	private List<MatchingAction> matchingAction = new ArrayList<MatchingAction>();
	
	private MATCHING_CONDITION matchingCondition;

//	@OneToMany
//    @JoinColumn(name="action_condition_id")
//	private List<MatchingActionCondition> matchingActionCondition = new ArrayList<MatchingActionCondition>();

//	@OneToOne
//    @JoinColumn(name="profile_id")
//	private AggregationProfile aggregationProfile;

}	
