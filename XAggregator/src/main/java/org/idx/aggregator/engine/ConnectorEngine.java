package org.idx.aggregator.engine;

import java.util.List;

import org.idx.aggregator.exception.InvalidProfileException;
import org.idx.aggregator.model.AggregationProfile;
import org.idx.aggregator.model.ConfigurationParams;
import org.idx.aggregator.model.Configurator;
import org.idx.aggregator.service.AggregationService;
import org.idx.aggregator.utils.IDXAggregatorContants;
import org.idx.core.engine.IDXContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConnectorEngine {
	
	@Autowired
	AggregationService aggregationService;
	
	@Autowired
	FlatFileConfigurator flatFileConfigurator;

	public void performReconciliation(IDXContext context, Configurator configurator, AggregationProfile profile) {

		String type = configurator.getType();
		
		if(type.equals(IDXAggregatorContants.FLATFILE))
			performFlatFileReconciliation(context, configurator, profile);
		else if(type.equals(IDXAggregatorContants.ACTIVE_DIRECTORY))
			performADReconciliation(configurator);
		else if(type.equals(IDXAggregatorContants.OFFICE_360))
			performOffice360Reconciliation(configurator);
		else if(type.equals(IDXAggregatorContants.WORKDAY))
			performWorkdayReconciliation(configurator);
		else if(type.equals(IDXAggregatorContants.LDAP))
			performLDAPReconciliation(configurator);
		else if(type.equals(IDXAggregatorContants.DATABASE))
			performDatabaseReconciliation(configurator);
		else if(type.equals(IDXAggregatorContants.GOOGLE_APPS))
			performGoogleAppsReconciliation(configurator);
		
		
	}

	private void performFlatFileReconciliation(IDXContext context, Configurator configurator, AggregationProfile profile) {

		List<ConfigurationParams> params = configurator.getConfigParams();
		
		try {
			flatFileConfigurator.performReconciliation(params, profile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	private void performADReconciliation(Configurator configurator) {
		// TODO Auto-generated method stub
		
	}

	private void performOffice360Reconciliation(Configurator configurator) {
		// TODO Auto-generated method stub
		
	}

	private void performWorkdayReconciliation(Configurator configurator) {
		// TODO Auto-generated method stub
		
	}

	private void performLDAPReconciliation(Configurator configurator) {
		// TODO Auto-generated method stub
		
	}

	private void performDatabaseReconciliation(Configurator configurator) {
		// TODO Auto-generated method stub
		
	}

	private void performGoogleAppsReconciliation(Configurator configurator) {
		// TODO Auto-generated method stub
		
	}


}
