package org.idx.aggregator.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.idx.application.dob.MATCHING_ACTION_CONDITION;

import lombok.Data;

@Entity
@Table(name = "AGG_MATCHING_ACTION")
@Data
public class MatchingAction {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private MATCHING_ACTION aggAction;
	
	private MATCHING_ACTION_CONDITION aggCondition;


}
