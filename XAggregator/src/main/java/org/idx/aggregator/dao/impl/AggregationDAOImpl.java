package org.idx.aggregator.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.idx.aggregator.dao.AggregationDAO;
import org.idx.aggregator.exception.AggregationServiceException;
import org.idx.aggregator.model.AggregationEvent;
import org.idx.aggregator.model.AggregationProfile;
import org.idx.aggregator.model.Application;
import org.idx.aggregator.model.ApplicationType;
import org.idx.aggregator.model.Configurator;
import org.idx.aggregator.model.EVENT_STATE;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class AggregationDAOImpl implements AggregationDAO {
	
    @PersistenceContext
    private EntityManager manager;

	public void createAggregationProfile(AggregationProfile profile) {
		manager.persist(profile);    	
	}

	public AggregationProfile getAggregationProfile(String profileName) {
		
		System.out.println("######### profileName ######### " + profileName);
		
		Query query = manager.createQuery("from AggregationProfile p where p.name = ?1", AggregationProfile.class);
		query.setParameter(1, profileName);
		AggregationProfile profile = (AggregationProfile)query.getSingleResult();
		return profile; 
	}
	
	public Application getApplication(String appName) throws AggregationServiceException{
		System.out.println("######### appName ######### " + appName);
		
		Query query = manager.createQuery("from Application p where p.name = ?1", Application.class);
		query.setParameter(1, appName);
		Application app = (Application)query.getSingleResult();
		
		System.out.println("######### app ######### " + app);

		
		return app; 

	}

	public void createSyncEvent(AggregationEvent event) {
		// TODO Auto-generated method stub
		
	}

	public List<AggregationEvent> getOpenEventsForProfile(String profileName) {
		// TODO Auto-generated method stub
		return null;
	}

	public void updateEventStatus(AggregationEvent event, EVENT_STATE eventState) {
		// TODO Auto-generated method stub
		
	}

	@Transactional
	public Application createApplication(Application application) {
		manager.persist(application);    	
		
		Query query = manager.createQuery("Select a from Application a where a.name= :name");
		query.setParameter("name", application.getName());
		
		System.out.println("application.getName : "+ application.getName());
		
		Application savedApplication = (Application) query.getSingleResult();

		return savedApplication;
	}

	public void creatConfigurator(Configurator configurator) {
		manager.persist(configurator);    	
		
	}

	public boolean checkIfApplicationNameExisits(String name) {
		Query query = manager.createQuery("Select a from Application a where a.name= :name");
		query.setParameter("name", name);
		
		List result = query.getResultList();
		
		if(result.size() > 0)
			return true;

		return false;
	}

	@Override
	public List<ApplicationType> getApplicationTypeList() {
		Query query = manager.createQuery("from ApplicationType p", ApplicationType.class);
		List<ApplicationType> appTypeList = (List<ApplicationType>)query.getResultList();
		return appTypeList;
	}

	@Transactional
	public ApplicationType createApplicationType(ApplicationType appType) {
		manager.persist(appType);   
		Query query = manager.createQuery("Select a from ApplicationType a where a.name= :name");
		query.setParameter("name", appType.getName());
		
		System.out.println("appType.getName : "+ appType.getName());
		
		ApplicationType savedAppType = (ApplicationType) query.getSingleResult();

		return savedAppType;
	}

}
