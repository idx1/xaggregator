package org.idx.aggregator.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

 
@Entity
@Table(name = "AGG_ATTR")
@Data
public class AggregationAttribute {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	
	private String name;
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="attr_id")
	private List<AggregationAttrValue> value;

	private ATTR_TYPE type;
	
	@ManyToOne
    @JoinColumn(name="event_id")
	private AggregationEvent aggregationEvent;

	
	
}
