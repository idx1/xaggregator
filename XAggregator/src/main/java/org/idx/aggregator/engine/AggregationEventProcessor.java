package org.idx.aggregator.engine;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.idx.aggregator.dob.AggregationUserMessage;
import org.idx.aggregator.jms.AggregationEngineMessageProducer;
import org.idx.aggregator.model.AGGREGATION_TYPE;
import org.idx.aggregator.model.AggregationAttrValue;
import org.idx.aggregator.model.AggregationAttribute;
import org.idx.aggregator.model.AggregationEvent;
import org.idx.aggregator.model.AggregationProfile;
import org.idx.aggregator.model.Application;
import org.idx.aggregator.model.MatchingAction;
import org.idx.aggregator.model.ProfileAttribute;
import org.idx.application.dob.AggregationProfileDOB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class AggregationEventProcessor {
	
	@Autowired
	AggregationEngineMessageProducer aggMessageProducer;
	
	private Mapper mapper = new DozerBeanMapper();

	
	public void processReconEvents(AggregationEvent event, AggregationProfile profile){
		
		
		
		  System.out.println("Received <" + event + ">");
		    System.out.println("Received <" + event.getUid() + ">");
		    System.out.println("Received <" + event.getEventState() + ">");
		    
 		    
		    AGGREGATION_TYPE aggType = profile.getAggregationType();
		    
		    System.out.println("profile : " + profile);
		    System.out.println("aggType : " + aggType);
		    
		    if(aggType.equals(AGGREGATION_TYPE.USER)) {
		    	performUserEventProcessing(event, profile);
		    }
		    
		    
	}

	private void performUserEventProcessing(AggregationEvent event, AggregationProfile profile) {
		
		Map<String, String> eventAttrMap = new HashMap<String, String>();
		
		List<AggregationAttribute> attrs = event.getAttributes();
		
		for(AggregationAttribute attr:attrs) {
			
			String name = attr.getName();
			String value = attr.getValue().get(0).getValue();
			eventAttrMap.put(name, value);
		}
		
		List<ProfileAttribute> profileAttrs = profile.getProfileAttr();
		
		AggregationUserMessage userMessage = new AggregationUserMessage();
		
		Map<String, String> userAttributeMap = new HashMap<String, String>();
		
		for(ProfileAttribute profileAttr: profileAttrs) {
			
			String userAttrName = profileAttr.getName();
			String tgtAttrName = profileAttr.getTargetAttrName();
			String userAttrValue = eventAttrMap.get(tgtAttrName);
			
			userAttributeMap.put(userAttrName, userAttrValue);
			
		}
		
		
		AggregationProfileDOB profileDOB =  
			    mapper.map(profile, AggregationProfileDOB.class);

		
		
		List<MatchingAction> matchingActions = profile.getMatchingActions();
		
		userMessage.setUserAttributeMap(userAttributeMap);
		userMessage.setMatchingActions(profileDOB.getMatchingActions());
		userMessage.setMatchingRule(profileDOB.getMatchingRule());
		
		aggMessageProducer.publishAggregationUserMessage(userMessage);
	}

}
