package org.idx.aggregator.service.impl;

import java.util.List;

import org.idx.aggregator.dao.ApplicationDAO;
import org.idx.aggregator.exception.AggregationServiceException;
import org.idx.aggregator.model.AccessProfile;
import org.idx.aggregator.model.Application;
import org.idx.aggregator.model.ApplicationProfile;
import org.idx.aggregator.model.Entitlement;
import org.idx.aggregator.service.ApplicationService;
import org.idx.application.dob.ApplicationDOB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ApplicationServiceImpl implements ApplicationService {

	@Autowired
	ApplicationDAO applicationDAO;
	
	@Override
	public Entitlement createEntitlement(Entitlement entitlement) throws AggregationServiceException {
		return applicationDAO.createEntitlement(entitlement);
	}

	@Override
	public Entitlement updateEntitlement(Entitlement entitlement) throws AggregationServiceException {
		return applicationDAO.updateEntitlement(entitlement);
	}

	@Override
	public void deleteEntitlement(long id) throws AggregationServiceException {
		applicationDAO.deleteEntitlement(id);
	}

	@Override
	public Entitlement findEntitlement(long id) throws AggregationServiceException {
		return applicationDAO.findEntitlement(id);
	}

	@Override
	public Entitlement findEntitlement(String name) throws AggregationServiceException {
		return applicationDAO.findEntitlement(name);
	}

	@Override
	public void enableEntitlement(long id) throws AggregationServiceException {
		applicationDAO.enableEntitlement(id);
	}

	@Override
	public void enableEntitlement(String name) throws AggregationServiceException {
		applicationDAO.enableEntitlement(name);
	}

	@Override
	public void disableEntitlement(long id) throws AggregationServiceException {
		applicationDAO.disableEntitlement(id);
	}

	@Override
	public void disableEntitlement(String name) throws AggregationServiceException {
		applicationDAO.disableEntitlement(name);
	}

	@Override
	public List<Entitlement> findEntitlementForApplication(String applicationName) throws AggregationServiceException {
		return applicationDAO.findEntitlementForApplication(applicationName);
	}

	@Override
	public List<Entitlement> findEntitlementForApplication(long applicationId) throws AggregationServiceException {
		return applicationDAO.findEntitlementForApplication(applicationId);
	}

	@Override
	public ApplicationProfile createApplicationProfile(ApplicationProfile applicationProfile)
			throws AggregationServiceException {
		return applicationDAO.createApplicationProfile(applicationProfile);
	}

	@Override
	public ApplicationProfile updateApplicationProfile(ApplicationProfile applicationProfile)
			throws AggregationServiceException {
		return applicationDAO.updateApplicationProfile(applicationProfile);
	}

	@Override
	public void deleteApplicationProfile(long id)
			throws AggregationServiceException {
		applicationDAO.deleteApplicationProfile(id);
	}

	@Override
	public ApplicationProfile findApplicationProfile(long id) throws AggregationServiceException {
		return applicationDAO.findApplicationProfile(id);
	}

	@Override
	public ApplicationProfile findApplicationProfile(String name) throws AggregationServiceException {
		return applicationDAO.findApplicationProfile(name);
	}

	@Override
	public void enableApplicationProfile(long id) throws AggregationServiceException {
		applicationDAO.enableApplicationProfile(id);
	}

	@Override
	public void enableApplicationProfile(String name) throws AggregationServiceException {
		applicationDAO.enableApplicationProfile(name);
	}

	@Override
	public void disableApplicationProfile(long id) throws AggregationServiceException {
		applicationDAO.disableApplicationProfile(id);
	}

	@Override
	public void disableApplicationProfile(String name) throws AggregationServiceException {
		applicationDAO.disableApplicationProfile(name);
	}

	@Override
	public List<ApplicationProfile> findApplicationProfileForApplication(String applicationName)
			throws AggregationServiceException {
		return applicationDAO.findApplicationProfileForApplication(applicationName);
	}

	@Override
	public List<ApplicationProfile> findApplicationProfileForApplication(long applicationId)
			throws AggregationServiceException {
		return applicationDAO.findApplicationProfileForApplication(applicationId);
	}

	@Override
	public AccessProfile createAccessProfile(AccessProfile accessProfile) throws AggregationServiceException {
		return applicationDAO.createAccessProfile(accessProfile);
	}

	@Override
	public AccessProfile updateAccessProfile(AccessProfile accessProfile) throws AggregationServiceException {
		return applicationDAO.updateAccessProfile(accessProfile);
	}

	@Override
	public void deleteAccessProfile(long id) throws AggregationServiceException {
		applicationDAO.deleteAccessProfile(id);
	}

	@Override
	public AccessProfile findAccessProfile(long id) throws AggregationServiceException {
		return applicationDAO.findAccessProfile(id);
	}

	@Override
	public AccessProfile findAccessProfile(String name) throws AggregationServiceException {
		return applicationDAO.findAccessProfile(name);
	}

	@Override
	public void enableAccessProfile(long id) throws AggregationServiceException {
		applicationDAO.enableAccessProfile(id);
	}

	@Override
	public void enableAccessProfile(String name) throws AggregationServiceException {
		applicationDAO.enableAccessProfile(name);
	}

	@Override
	public void disableAccessProfile(long id) throws AggregationServiceException {
		applicationDAO.disableAccessProfile(id);
	}

	@Override
	public void disableAccessProfile(String name) throws AggregationServiceException {
		applicationDAO.disableAccessProfile(name);
	}

	@Override
	public Application findApplication(String name) throws AggregationServiceException{

		return applicationDAO.findApplication(name);
	}

	@Override
	public List<Application> getApplicationList() throws AggregationServiceException {
		return applicationDAO.getApplicationList();
	}


}
