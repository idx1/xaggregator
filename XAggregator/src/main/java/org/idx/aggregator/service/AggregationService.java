package org.idx.aggregator.service;

import java.util.List;

import org.idx.aggregator.exception.AggregationServiceException;
import org.idx.aggregator.model.AggregationEvent;
import org.idx.aggregator.model.AggregationProfile;
import org.idx.aggregator.model.Application;
import org.idx.aggregator.model.ApplicationType;
import org.idx.aggregator.model.Configurator;
import org.idx.aggregator.model.EVENT_STATE;
import org.idx.application.dob.ApplicationTypeDOB;
import org.idx.core.engine.IDXContext;

public interface AggregationService {
	
    public int getNextSequence(String seqName) throws AggregationServiceException;

	public void createAggregationProfile(AggregationProfile profile) throws AggregationServiceException;

	public AggregationProfile getAggregationProfile(String profileName) throws AggregationServiceException;

	public AggregationEvent createAggregationEvent(AggregationEvent event, String profileName) throws AggregationServiceException;

	public List<AggregationEvent> getOpenEventsForProfile(String profileName) throws AggregationServiceException;

	public void updateEventStatus(AggregationEvent event, EVENT_STATE eventState) throws AggregationServiceException;

	public Application createApplication(Application application) throws AggregationServiceException;

	public void creatConfigurator(Configurator flatfileConfigurator) throws AggregationServiceException;
	
	public void performReconciliation(IDXContext context, Application application) throws AggregationServiceException;

	public List<ApplicationType> getApplicationTypeList() throws AggregationServiceException;

	public ApplicationType createApplicationType(ApplicationType appType) throws AggregationServiceException;

	public Application getApplication(String appName) throws AggregationServiceException;
	

}
