package org.idx.aggregator.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "ACCESS_PROFILE")
@Data
public class AccessProfile {

	@Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    private long id;
    
	@Column(unique = true)
    private String name;

    private String description;
    
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="app_profile_id")
    ApplicationProfile applicationProfile;
    
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="entitlement_id")
    private Entitlement entitlement;

	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="app_role_id")
    private ApplicationRole appRole;
	
    private boolean valid;
	
    private String createdBy;

    private String updatedBy;

    private Date createdDate;
    
    private Date updatedDate;


}
