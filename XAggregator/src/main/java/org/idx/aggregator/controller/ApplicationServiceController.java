package org.idx.aggregator.controller;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.idx.aggregator.exception.AggregationServiceException;
import org.idx.aggregator.model.AccessProfile;
import org.idx.aggregator.model.Application;
import org.idx.aggregator.model.ApplicationProfile;
import org.idx.aggregator.model.Entitlement;
import org.idx.aggregator.service.ApplicationService;
import org.idx.application.dob.AccessProfileDOB;
import org.idx.application.dob.ApplicationDOB;
import org.idx.application.dob.ApplicationProfileDOB;
import org.idx.application.dob.ApplicationTypeDOB;
import org.idx.application.dob.EntitlementDOB;
import org.idx.common.rest.ServerResponse;
import org.idx.common.rest.ServerResponseCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/application/")
public class ApplicationServiceController {
	
	@Autowired
	private ApplicationService applicationService;

	private Mapper mapper = new DozerBeanMapper();

	@PostMapping("/createEntitlement")
	public ResponseEntity<Object> createEntitlement(@RequestBody EntitlementDOB entitlementDOB) 
			throws AggregationServiceException{
		Entitlement entitlement = (Entitlement) mapObject(entitlementDOB, Entitlement.class);
		
		System.out.println("entitlementDOB : "+ entitlementDOB);
		System.out.println("entitlement : " + entitlement);
		
		Entitlement savedEnt = null;
		
		savedEnt = applicationService.createEntitlement(entitlement);
			
 		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedEnt.getId()).toUri();

		return ResponseEntity.created(location).build();

	}

	@GetMapping("/findEntitlementById")
	public ServerResponse findEntitlement(@RequestParam("id") long id) throws AggregationServiceException{
		
		Entitlement ent = applicationService.findEntitlement(id);

		EntitlementDOB entDOB = (EntitlementDOB) mapObject(ent, EntitlementDOB.class);

		return getServerResponse(ServerResponseCode.SUCCESS, entDOB);	
	}
	
	@GetMapping("/findEntitlementByName")
	public ServerResponse findEntitlement(@RequestParam("name")String name) throws AggregationServiceException{
		Entitlement ent = applicationService.findEntitlement(name);
		
		EntitlementDOB entDOB = (EntitlementDOB) mapObject(ent, EntitlementDOB.class);
		
		return getServerResponse(ServerResponseCode.SUCCESS, entDOB);	

	}

	@GetMapping("/findEntitlementForAppByName")
	public ServerResponse findEntitlementForApplication(@RequestParam("applicationName")String  applicationName) throws AggregationServiceException{
		List<Entitlement> entList = applicationService.findEntitlementForApplication(applicationName);

		List<EntitlementDOB> entDOBs = mapList(entList, EntitlementDOB.class);
		
		return getServerResponse(ServerResponseCode.SUCCESS, entDOBs);	

	}
	
	@GetMapping("/findEntitlementForAppById")
	public ServerResponse findEntitlementForApplication(@RequestParam("applicationId") long applicationId) throws AggregationServiceException{
		List<Entitlement> entList = applicationService.findEntitlementForApplication(applicationId);

		List<EntitlementDOB> entDOBs = mapList(entList, EntitlementDOB.class);
		
		return getServerResponse(ServerResponseCode.SUCCESS, entDOBs);	
	}

	@PostMapping("/createApplicationProfile")
	public ApplicationProfileDOB createApplicationProfile(@RequestBody ApplicationProfileDOB applicationProfileDOB) throws AggregationServiceException {
		
		ApplicationProfile applicationProfile = (ApplicationProfile) mapObject(applicationProfileDOB, ApplicationProfile.class);
		
		System.out.println("applicationProfile : "+ applicationProfile);
		System.out.println("applicationProfileDOB : " + applicationProfileDOB);
		
 		
		ApplicationProfile savedAppProfile;
		ApplicationProfileDOB savedAppProfileDOB;
		 
			savedAppProfile = applicationService.createApplicationProfile(applicationProfile);
			savedAppProfileDOB = (ApplicationProfileDOB) mapObject(savedAppProfile, ApplicationProfileDOB.class);;

		 
			
// 		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
//				.buildAndExpand(savedEnt.getId()).toUri();

		return savedAppProfileDOB;


	}
	
	@GetMapping("/getApplicationList")
	public ApplicationDOB[] getApplicationList() throws AggregationServiceException{
		List<Application> apps = applicationService.getApplicationList();
				
				List<ApplicationDOB> appDobs = mapList(apps, ApplicationDOB.class);
				
				ApplicationDOB[] appList = new  ApplicationDOB[apps.size()];	
		
		int i=0;
		for(ApplicationDOB app:appDobs) {
			
			appList[i] = app;
			
			i++;
		}
		
		return appList;
				
	}

	
	
	
	@GetMapping("/getEntitlementsForApplication")
	public EntitlementDOB[] getEntitlementsForApplication(@RequestParam("name") String name) throws AggregationServiceException{
		
		System.out.println("name : "+ name);
		
		String decodedName = null;
		try {
			decodedName = URLDecoder.decode(name, "UTF-8");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		System.out.println("naam --> "+ name);
		System.out.println("decodedName --> "+ decodedName);


		List<Entitlement> ents = applicationService.findEntitlementForApplication(decodedName);
		
		
		//System.out.println("ents : "+ ents);

				
				List<EntitlementDOB> entDobs = mapList(ents, EntitlementDOB.class);
				
				EntitlementDOB[] entList = new  EntitlementDOB[ents.size()];	
		
		int i=0;
		for(EntitlementDOB ent:entDobs) {
			
			entList[i] = ent;
			
			i++;
		}
		
		return entList;
				
	}

	
	@GetMapping("/findApplicationProfileById")
	public ServerResponse findApplicationProfile(long id) {
		ApplicationProfile profile = null;
		ApplicationProfileDOB profileDOB = null;
		try {
			profile = applicationService.findApplicationProfile(id);
			profileDOB = (ApplicationProfileDOB) mapObject(profile, ApplicationProfileDOB.class);
		} catch (AggregationServiceException e) {
			e.printStackTrace();
			return getServerResponse(ServerResponseCode.ERROR, e);	
		}
		return getServerResponse(ServerResponseCode.SUCCESS, profileDOB);	
	}
	
	@GetMapping("/findApplicationProfileByName")
	public ServerResponse findApplicationProfile(String name) {
		String decodedName = null;
		try {
			decodedName = URLDecoder.decode(name, "UTF-8");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		System.out.println("naam --> "+ name);
		System.out.println("decodedName --> "+ decodedName);

		ApplicationProfile profile = null;
		ApplicationProfileDOB profileDOB = null;
		try {
			profile = applicationService.findApplicationProfile(decodedName);
			profileDOB = (ApplicationProfileDOB) mapObject(profile, ApplicationProfileDOB.class);
		} catch (AggregationServiceException e) {
			e.printStackTrace();
			return getServerResponse(ServerResponseCode.ERROR, e);	
		}
		return getServerResponse(ServerResponseCode.SUCCESS, profileDOB);			
	}
	

	@GetMapping("/findAppProfilesForAppByName")
	public ServerResponse findApplicationProfileForApplication(String name) {
		
		
		String decodedName = null;
		try {
			decodedName = URLDecoder.decode(name, "UTF-8");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		System.out.println("naam --> "+ name);
		System.out.println("decodedName --> "+ decodedName);

		
		List<ApplicationProfile> appProfiles = null;
		List<ApplicationProfileDOB> appProfileDOBs = null;
		try {
			appProfiles = applicationService.findApplicationProfileForApplication(decodedName);
			appProfileDOBs = mapList(appProfiles, ApplicationProfileDOB.class);
		} catch (AggregationServiceException e) {
			e.printStackTrace();
			return getServerResponse(ServerResponseCode.ERROR, e);	
		}
		return getServerResponse(ServerResponseCode.SUCCESS, appProfileDOBs);			
	}
	
	@GetMapping("/findAppProfilesForAppById")
	public ServerResponse findApplicationProfileForApplication(long applicationId) {
		List<ApplicationProfile> appProfiles = null;
		List<ApplicationProfileDOB> appProfileDOBs = null;
		try {
			appProfiles = applicationService.findApplicationProfileForApplication(applicationId);
			appProfileDOBs = mapList(appProfiles, ApplicationProfileDOB.class);
		} catch (AggregationServiceException e) {
			e.printStackTrace();
			return getServerResponse(ServerResponseCode.ERROR, e);	
		}
		return getServerResponse(ServerResponseCode.SUCCESS, appProfileDOBs);			
	}

	@PostMapping("/createAccessProfile")
	public ServerResponse createAccessProfile(@RequestBody AccessProfileDOB accessProfileDOB) {
		
	 
		AccessProfile accessProfile = (AccessProfile) mapObject(accessProfileDOB, AccessProfile.class);
		
		System.out.println("accessProfile : "+ accessProfile);
		System.out.println("accessProfileDOB : " + accessProfileDOB);
		
 		
		AccessProfile savedAccessProfile;
		AccessProfileDOB savedAccessProfileDOB;
		 
		try {
			savedAccessProfile = applicationService.createAccessProfile(accessProfile);
			savedAccessProfileDOB =  (AccessProfileDOB) mapObject(savedAccessProfile, AccessProfileDOB.class);
		} catch (AggregationServiceException e) {
			e.printStackTrace();
			return getServerResponse(ServerResponseCode.ERROR, e);	
		}
		
		return getServerResponse(ServerResponseCode.SUCCESS, savedAccessProfileDOB);			
	}

	@GetMapping("/findAccessProfileById")
	public ServerResponse findAccessProfile(long id) {
		AccessProfile profile = null;
		AccessProfileDOB profileDOB = null;
		try {
			profile = applicationService.findAccessProfile(id);
			profileDOB = (AccessProfileDOB) mapObject(profile, AccessProfileDOB.class);
		} catch (AggregationServiceException e) {
			e.printStackTrace();
			return getServerResponse(ServerResponseCode.ERROR, e);	
		}
		return getServerResponse(ServerResponseCode.SUCCESS, profileDOB);			

	}
	
	@GetMapping("/findAccessProfileByName")
	public ServerResponse findAccessProfile(@RequestParam("name") String name) {
		
		
		String decodedName = null;
		try {
			decodedName = URLDecoder.decode(name, "UTF-8");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		System.out.println("naam --> "+ name);
		System.out.println("decodedName --> "+ decodedName);

		AccessProfile profile = null;
		AccessProfileDOB profileDOB = null;
		try {
			
			
			profile = applicationService.findAccessProfile(decodedName);
			profileDOB = (AccessProfileDOB) mapObject(profile, AccessProfileDOB.class);
		} catch (AggregationServiceException e) {
			e.printStackTrace();
			return getServerResponse(ServerResponseCode.ERROR, e);	
		}
		return getServerResponse(ServerResponseCode.SUCCESS, profileDOB);			

	}    
	
	
	@GetMapping("/findApplicationByName")
 	public ApplicationDOB findApplicationByName(@RequestParam("name") String name) throws AggregationServiceException {
		
		System.out.println("naam --> "+ name);
		
		String decodedName = null;
		try {
			decodedName = URLDecoder.decode(name, "UTF-8");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Application app = null;
		 
			app = applicationService.findApplication(decodedName);
		 
		
		
		System.out.println("app --> "+ app);

		ApplicationDOB appDOB = (ApplicationDOB) mapObject(app, ApplicationDOB.class);
		
		System.out.println("appDOB --> "+ appDOB);

		return appDOB;
	}
	
	
//	@GetMapping("/findApplicationByName/{name}")
//	public ServerResponse findTest(@PathVariable String name) {
//		System.out.println("naam --> "+ name);
//
//		Application app = null;
//		try {
//			app = applicationService.findApplication(name);
//		} catch (AggregationServiceException e) {
//			e.printStackTrace();
//			getServerResponse(ServerResponseCode.ERROR, e);
//		}
//		
//		ApplicationDOB entDOB = (ApplicationDOB) mapObject(app, ApplicationDOB.class);
//		
//		return getServerResponse(ServerResponseCode.SUCCESS, entDOB);	
//	}


	public ServerResponse getServerResponse(int responseCode, Object data){
		ServerResponse serverResponse = new ServerResponse();
		serverResponse.setStatusCode(responseCode);
		serverResponse.setData(data);
		return serverResponse; 
	}

	private <Entitlement, EntitlementDOB> List<EntitlementDOB> mapEntitlementList(List<Entitlement> fromList, final Class<EntitlementDOB> toClass) {		
		Mapper mapper1 = new DozerBeanMapper();

	    return fromList
	            .stream()
	            .map(from -> this.mapper.map(from, toClass))
	            .collect(Collectors.toList());
	}

	<FROM, TO> List<TO> mapList(List<FROM> fromList, final Class<TO> toClass) {
	    return fromList
	            .stream()
	            .map(from -> this.mapper.map(from, toClass))
	            .collect(Collectors.toList());
	}
	
	static<FROM, TO>  Object mapObject(Object obj, Class<TO> objClass){
		
		Mapper mapper = new DozerBeanMapper();

		Object retObj = mapper.map(obj, objClass);
		
		return retObj;
		
	}
	
	public static void main(String args[]) {
		
		Entitlement ent1 = new Entitlement();
		ent1.setName("Test");
		Application application = new Application();
		application.setName("aap1");
		ent1.setApplication(application );
		
		EntitlementDOB entDOC = (EntitlementDOB) mapObject(ent1, EntitlementDOB.class);
		
		System.out.println("ent1 : "+ ent1);
		System.out.println("entDOC : " + entDOC);
		

//		List<Entitlement> entList = new ArrayList<Entitlement>();
//		
//		Entitlement ent1 = new Entitlement();
//		ent1.setName("Duaon me yaad rakhna");
//
//		Entitlement ent2 = new Entitlement();
//		ent2.setName("Duaon me yaad mat rakhna");
//
//		entList.add(ent1);
//		
//		Mapper mapper1 = new DozerBeanMapper();
// 
//		List<EntitlementDOB> ents =  
//			    mapper1.map(entList, new ArrayList<EntitlementDOB>().getClass());
//		
//		ents = mapList(entList, EntitlementDOB.class);
//		
//		System.out.println("entList : " + entList);
//		System.out.println("ents : " + ents);
//
		
	}
	
	

}
