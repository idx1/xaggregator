package org.idx.aggregator.model;

public enum AGGREGATION_TYPE {

	USER (1),
	ACCOUNT (2),
	ROLE (3),
	ENTITLEMENT (4);

	private final int aggregationType;

	AGGREGATION_TYPE(int aggregationType){
		this.aggregationType=aggregationType;
	}

	public int getAggregationType(){
		return aggregationType;
	}

}


 