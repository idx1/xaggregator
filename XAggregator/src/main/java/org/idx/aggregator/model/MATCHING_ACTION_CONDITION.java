package org.idx.aggregator.model;

public enum MATCHING_ACTION_CONDITION {
	NO_MATCH (1),
	SINGLE_MATCH (2),
	MULTIPLE_MATCH(3);
	
	private final int  matchingAction;
	
	MATCHING_ACTION_CONDITION(int matchingAction){
		this.matchingAction=matchingAction;
	}
	
	public int getMatchingAction(){
		return this.matchingAction;
	}
}
