package org.idx.aggregator.jms;

import org.idx.aggregator.dob.AggregationEventDOB;
import org.idx.aggregator.engine.AggregationEventProcessor;
import org.idx.aggregator.model.AggregationEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class AggregationEngineMessageListener {
	
	@Autowired
	AggregationEventProcessor eventProcessor;

	  @JmsListener(destination = "AggregationOperationMessageQueue", containerFactory = "IDXFactory")
	  public void receiveMessage(AggregationEventDOB aggEvent) {

		  System.out.println("AggregationEngineMessageListener with AggregationEventDOB : " + aggEvent);

//		    eventProcessor.processReconEvents(aggEvent);
	  }

}
