package org.idx.aggregator.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.identityconnectors.framework.common.objects.Attribute;
import org.identityconnectors.framework.common.objects.ConnectorObject;
import org.identityconnectors.framework.common.objects.Uid;
import org.idx.aggregator.model.AggregationAttrValue;
import org.idx.aggregator.model.AggregationAttribute;
import org.idx.aggregator.model.AggregationEvent;
import org.idx.aggregator.model.EVENT_STATE;

 
public class ConnectorUtils {

	public static AggregationEvent getAggregationEvent(ConnectorObject obj, String profileName) {
		
		validateConnectorObject(obj, profileName);
		
		transformConnectorObject(obj, profileName);
 
		AggregationEvent aggregationEvent = new AggregationEvent();
		List<AggregationAttribute> attributes = new ArrayList<AggregationAttribute>();
		
		Set<Attribute> attrs = obj.getAttributes();
		
		Iterator<Attribute> itr = attrs.iterator();
		
		Uid uid = obj.getUid();
		
		String uidName = uid.getName();
		String uidValue = uid.getUidValue();
		
		aggregationEvent.setUid(uidName);
		aggregationEvent.setUidValue(uidValue);
		
		while(itr.hasNext()){			
			Attribute attr = itr.next();

			String name = attr.getName();

			//System.out.println("attr : name : "+ name);
			
			List<Object> values = attr.getValue();
			List<AggregationAttrValue> strValues = new ArrayList<AggregationAttrValue>();
			AggregationAttribute aggregationAttr = new AggregationAttribute();

			for(Object value: values){
				
				AggregationAttrValue attrValue = new AggregationAttrValue();
				
				String strValue = (String)value;
				
				attrValue.setValue(strValue);
				//attrValue.setAggregationAttr(aggregationAttr);
				
				strValues.add(attrValue);
			}
			
			aggregationAttr.setName(name);
			aggregationAttr.setValue(strValues);
			
			attributes.add(aggregationAttr);
			
		}
		
		aggregationEvent.setAttributes(attributes);
		aggregationEvent.setEventState(EVENT_STATE.PENDING);
		aggregationEvent.setCreationDate(new Date());
 		
		
		return aggregationEvent;
	}

	private static void transformConnectorObject(ConnectorObject obj, String profileName) {
		// TODO Need to think about Transformation framework
		
	}

	private static boolean validateConnectorObject(ConnectorObject obj, String profileName) {
		// TODO Need to think about validation framework
		
		return true;
	}

	public static void publishAggregationEvent(AggregationEvent retEvent) {

		
	}

}
