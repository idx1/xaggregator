package org.idx.aggregator.dao.impl;

import org.idx.aggregator.dao.AggregationNOSQLDAO;
import org.idx.aggregator.model.AggregationEvent;
import org.idx.aggregator.model.EVENT_STATE;
import org.idx.aggregator.model.IDXNoSqlSequences;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;
import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Update;

@Repository
public class AggregationNOSQLDAOImpl implements AggregationNOSQLDAO{
	
	@Autowired
	private MongoTemplate mongoTemplate;

    @Autowired private MongoOperations mongo;

    public int getNextSequence(String seqName)
    {
    	IDXNoSqlSequences counter = mongo.findAndModify(
            query(where("_id").is(seqName)),
            new Update().inc("seq",1),
            options().returnNew(true).upsert(true),
            IDXNoSqlSequences.class);
        return counter.getSeq();
    }

	public AggregationEvent createAggregationEvent(AggregationEvent event) {
		mongoTemplate.insert(event);
		return event;
	}

	public void updateEventStatus(AggregationEvent event, EVENT_STATE eventState) {

	}

	public AggregationEvent getAggregationEvent(long id) {
		return null;
	}

	public AggregationEvent getAllEventsForProfile(String profileName) {
		return null;
	}

	public AggregationEvent getAllEventForAggregation(String aggregationId) {
		return null;
	}

}
