package org.idx.aggregator.service.impl;

import java.util.List;

import org.idx.aggregator.jms.AggregationEngineMessageProducer;
import org.idx.aggregator.common.IDXExceptionMessages;
import org.idx.aggregator.dao.AggregationDAO;
import org.idx.aggregator.dao.AggregationNOSQLDAO;
import org.idx.aggregator.engine.AggregationEventProcessor;
import org.idx.aggregator.engine.ConnectorEngine;
import org.idx.aggregator.exception.AggregationServiceException;
import org.idx.aggregator.model.AggregationEvent;
import org.idx.aggregator.model.AggregationProfile;
import org.idx.aggregator.model.Application;
import org.idx.aggregator.model.ApplicationType;
import org.idx.aggregator.model.Configurator;
import org.idx.aggregator.model.EVENT_STATE;
import org.idx.aggregator.service.AggregationService;
import org.idx.aggregator.utils.ConnectorUtils;
import org.idx.core.engine.IDXContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AggregationServiceImpl implements AggregationService{

	
	@Autowired
	AggregationNOSQLDAO nosqlDao;
	
	@Autowired
	AggregationDAO sqlDao;
	
	@Autowired
	ConnectorEngine connectorEngine;
	
	@Autowired
	AggregationEngineMessageProducer aggregationEngineMessageProducer;
	
	@Autowired
	AggregationEventProcessor eventProcessor;
	
	
    public int getNextSequence(String seqName) {
    	return nosqlDao.getNextSequence(seqName);
    }
	
	public void createAggregationProfile(AggregationProfile profile) {
		sqlDao.createAggregationProfile(profile);		
	}

	public AggregationProfile getAggregationProfile(String profileName) {
		return sqlDao.getAggregationProfile(profileName);
	}

	public AggregationEvent createAggregationEvent(AggregationEvent event, 
			String profileName) {
		
		AggregationEvent retEvent = nosqlDao.createAggregationEvent(event);	
		
		AggregationProfile profile = sqlDao.getAggregationProfile(profileName);
	
		eventProcessor.processReconEvents(retEvent, profile);
		
		return retEvent;		
	}

	public List<AggregationEvent> getOpenEventsForProfile(String profileName) {
		// TODO Auto-generated method stub
		return null;
	}

	public void updateEventStatus(AggregationEvent event, EVENT_STATE eventState) {
		// TODO Auto-generated method stub
		
	}

	public Application createApplication(Application application) throws AggregationServiceException {
		
		boolean exists = sqlDao.checkIfApplicationNameExisits(application.getName());
		
		if(exists)
			throw new AggregationServiceException(IDXExceptionMessages.APPLICATION_NAME_AREADY_EXISTS);

		Application savedApplication = sqlDao.createApplication(application);
		return savedApplication;
	}

	public void creatConfigurator(Configurator configurator) {
		sqlDao.creatConfigurator(configurator);		
	}

	public void performReconciliation(IDXContext context, Application application) {
		
		Configurator configurator = application.getConfigurator();
		
		String profileName = configurator.getProfileName();
		
		AggregationProfile profile =  application.getProfile();


 		
		connectorEngine.performReconciliation(context, configurator, profile);
		
		
		
	}
	
	public Application getApplication(String appName) throws AggregationServiceException{
		
		return sqlDao.getApplication(appName);
	}

	@Override
	public List<ApplicationType> getApplicationTypeList() {
		return sqlDao.getApplicationTypeList();
	}

	@Override
	public ApplicationType createApplicationType(ApplicationType appType) throws AggregationServiceException {
				return sqlDao.createApplicationType(appType);
	}

 
}
