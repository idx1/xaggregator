package org.idx.aggregator.dao;

import java.util.List;

import org.idx.aggregator.exception.AggregationServiceException;
import org.idx.aggregator.model.*;

 
public interface AggregationDAO {
	
	public Application createApplication(Application application);

	public void createAggregationProfile(AggregationProfile profile) ;

	public AggregationProfile getAggregationProfile(String profileName) ;

	public void createSyncEvent(AggregationEvent event) ;

	public List<AggregationEvent> getOpenEventsForProfile(String profileName);

	public void updateEventStatus(AggregationEvent event, EVENT_STATE eventState);

	public void creatConfigurator(Configurator configurator);

	public boolean checkIfApplicationNameExisits(String name);

	public List<ApplicationType> getApplicationTypeList();

	public ApplicationType createApplicationType(ApplicationType appType);

	public Application getApplication(String appName) throws AggregationServiceException;

}
