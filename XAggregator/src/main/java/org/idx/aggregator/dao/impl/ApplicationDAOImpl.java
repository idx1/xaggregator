package org.idx.aggregator.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.idx.aggregator.dao.ApplicationDAO;
import org.idx.aggregator.exception.AggregationServiceException;
import org.idx.aggregator.model.AccessProfile;
import org.idx.aggregator.model.AggregationProfile;
import org.idx.aggregator.model.Application;
import org.idx.aggregator.model.ApplicationProfile;
import org.idx.aggregator.model.Entitlement;
import org.idx.application.dob.ApplicationDOB;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class ApplicationDAOImpl implements ApplicationDAO{

    @PersistenceContext
    private EntityManager manager;

    @Transactional
	public Entitlement createEntitlement(Entitlement entitlement) throws AggregationServiceException {
		manager.persist(entitlement);    	
		return findEntitlement(entitlement.getName());
	}

	@Override
	public Entitlement updateEntitlement(Entitlement entitlement) throws AggregationServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteEntitlement(long id) throws AggregationServiceException {
	}

	@Override
	public Entitlement findEntitlement(long id) throws AggregationServiceException {
		Query query = manager.createQuery("from Entitlement p where p.id = ?1", AggregationProfile.class);
		query.setParameter(1, id);
		Entitlement entitlement = (Entitlement)query.getSingleResult();
		return entitlement; 
	}

	@Override
	public Entitlement findEntitlement(String name) throws AggregationServiceException {
		Query query = manager.createQuery("from Entitlement p where p.name = ?1", Entitlement.class);
		query.setParameter(1, name);
		Entitlement entitlement = (Entitlement)query.getSingleResult();
		return entitlement; 
	}

	@Override
	public void enableEntitlement(long id) throws AggregationServiceException {
 		
	}

	@Override
	public void enableEntitlement(String name) throws AggregationServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disableEntitlement(long id) throws AggregationServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disableEntitlement(String name) throws AggregationServiceException {
		// TODO Auto-generated method stub
		
	}

	@Transactional
	public ApplicationProfile createApplicationProfile(ApplicationProfile applicationProfile)
			throws AggregationServiceException {
		
		
		System.out.println("applicationProfile : "+ applicationProfile);
		
		manager.persist(applicationProfile);    	
		return findApplicationProfile(applicationProfile.getName());
	}

	@Override
	public ApplicationProfile updateApplicationProfile(ApplicationProfile applicationProfile)
			throws AggregationServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteApplicationProfile(long id)
			throws AggregationServiceException {
	}

	@Override
	public ApplicationProfile findApplicationProfile(long id) throws AggregationServiceException {
		Query query = manager.createQuery("from ApplicationProfile p where p.id = ?1", ApplicationProfile.class);
		query.setParameter(1, id);
		ApplicationProfile applicationProfile = (ApplicationProfile)query.getSingleResult();
		return applicationProfile; 
	}

	@Override
	public ApplicationProfile findApplicationProfile(String name) throws AggregationServiceException {
		Query query = manager.createQuery("from ApplicationProfile p where p.name = ?1", ApplicationProfile.class);
		query.setParameter(1, name);
		ApplicationProfile applicationProfile = (ApplicationProfile)query.getSingleResult();
		return applicationProfile; 
	}

	@Override
	public void enableApplicationProfile(long id) throws AggregationServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void enableApplicationProfile(String name) throws AggregationServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disableApplicationProfile(long id) throws AggregationServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disableApplicationProfile(String name) throws AggregationServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public AccessProfile createAccessProfile(AccessProfile accessProfile) throws AggregationServiceException {
		manager.persist(accessProfile);    	
		return findAccessProfile(accessProfile.getName());
	}

	@Override
	public AccessProfile updateAccessProfile(AccessProfile accessProfile) throws AggregationServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteAccessProfile(long id) throws AggregationServiceException {
	}

	@Override
	public AccessProfile findAccessProfile(long id) throws AggregationServiceException {
		Query query = manager.createQuery("from AccessProfile p where p.id = ?1", AccessProfile.class);
		query.setParameter(1, id);
		AccessProfile accessProfile = (AccessProfile)query.getSingleResult();
		return accessProfile; 
	}

	@Override
	public AccessProfile findAccessProfile(String name) throws AggregationServiceException {
		Query query = manager.createQuery("from AccessProfile p where p.name = ?1", AccessProfile.class);
		query.setParameter(1, name);
		AccessProfile accessProfile = (AccessProfile)query.getSingleResult();
		return accessProfile; 
	}

	@Override
	public void enableAccessProfile(long id) throws AggregationServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void enableAccessProfile(String name) throws AggregationServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disableAccessProfile(long id) throws AggregationServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disableAccessProfile(String name) throws AggregationServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Entitlement> findEntitlementForApplication(String applicationName) throws AggregationServiceException {
		
		System.out.println("applicationName : " + applicationName);

		
		Query query = manager.createQuery("from Entitlement p where p.application.name = ?1", Entitlement.class);
		query.setParameter(1, applicationName);
		List<Entitlement> entitlements = query.getResultList();
		//System.out.println("entitlements : " + entitlements);

		return entitlements;
	}

	@Override
	public List<Entitlement> findEntitlementForApplication(long applicationId) throws AggregationServiceException {
		Query query = manager.createQuery("from Entitlement p where p.application.id = ?1", Entitlement.class);
		query.setParameter(1, applicationId);
		List<Entitlement> entitlements = query.getResultList();
		return entitlements;
	}

	@Override
	public List<ApplicationProfile> findApplicationProfileForApplication(String applicationName)
			throws AggregationServiceException {
		Query query = manager.createQuery("from ApplicationProfile p where p.application.name = ?1", ApplicationProfile.class);
		query.setParameter(1, applicationName);
		List<ApplicationProfile> applicationProfiles = query.getResultList();
		return applicationProfiles;
	}

	@Override
	public List<ApplicationProfile> findApplicationProfileForApplication(long applicationId)
			throws AggregationServiceException {
		Query query = manager.createQuery("from ApplicationProfile p where p.application.id = ?1", ApplicationProfile.class);
		query.setParameter(1, applicationId);
		List<ApplicationProfile> applicationProfiles = query.getResultList();
		return applicationProfiles;
	}

	@Override
	public Application findApplication(String name) throws AggregationServiceException {
		
		
		System.out.println("name : " + name);
		
		Query query = manager.createQuery("from Application p where p.name = ?1", Application.class);
		query.setParameter(1, name);
		Application application = (Application)query.getSingleResult();
		
		
		System.out.println("application : " + application);

		
		return application; 
	}

	@Override
	public List<Application> getApplicationList() throws AggregationServiceException {
		Query query = manager.createQuery("from Application p", Application.class);
		List<Application> applications = query.getResultList();
		return applications;
	}


 
}
