package org.idx.aggregator.jms;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.idx.aggregator.dob.AggregationEventDOB;
import org.idx.aggregator.dob.AggregationUserMessage;
import org.idx.aggregator.model.AggregationEvent;
import org.idx.aggregator.model.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class AggregationEngineMessageProducer {

	  @Autowired 
	  private JmsTemplate jmsTemplate;

		private Mapper mapper = new DozerBeanMapper();

    @Async
	public void publishAggregationEvent(AggregationEvent aggEvent) {
    	
    	
    	AggregationEventDOB aggEventDOB =  
			    mapper.map(aggEvent, AggregationEventDOB.class);

    	System.out.println("########### PUBLISHING : " + aggEventDOB);

    	System.out.println("########### PUBLISHING : " + jmsTemplate.getDefaultDestination());
    	System.out.println("########### PUBLISHING : " + jmsTemplate.getDeliveryMode());
    	System.out.println("########### PUBLISHING : " + jmsTemplate.getDestinationResolver());
    	System.out.println("########### PUBLISHING : " + jmsTemplate.getDefaultDestinationName());
    	System.out.println("########### PUBLISHING : " + jmsTemplate.isPubSubNoLocal());

    	

		jmsTemplate.convertAndSend("AggregationOperationMessageQueue",aggEventDOB);

	}

	public void publishAggregationUserMessage(AggregationUserMessage userMessage) {

		jmsTemplate.convertAndSend("AggregationOperationMessageQueue",userMessage);

	}

	  
}
