package org.idx.aggregator.logging;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.idx.aggregator.engine.FlatFileConfigurator;

public class IDXLogger {

	private Logger logger;
	
	private String loggerName;

	public IDXLogger(String loggerName) {
		logger = LogManager.getLogger(loggerName);
	}

	public IDXLogger(Class<?> loggerClass) {
		logger = LogManager.getLogger(this.getClass());
	}

	public void error(String message) {
		System.out.println("ERROR: " + loggerName + " : " + message);
 		
	}

	public void debug(String message) {
		logger.debug(message);
	}

	
	public void info(String message) {
		System.out.println("DEBUG: " + loggerName + " : " + message);
	}

	
	public void notification(String message) {
		System.out.println("DEBUG: " + loggerName + " : " + message);
	}

}
