package org.idx.aggregator.model;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

import javax.persistence.Id;

@Entity
@Table(name = "APPLICATION")
@Data
public class Application {
	
	@Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    private long id;
    
	@Column(unique = true)
    private String name;
    
    private String displayName;

    private String description;
    
    private boolean isTrusted;
    
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="config_id")    
    private Configurator configurator;
    
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="profile_id")
    private AggregationProfile profile;

//	@OneToMany(cascade=CascadeType.ALL,mappedBy="application")
//    private Set<Entitlement> entitlements;

//	public long getId() {
//		return id;
//	}
//
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public String getDisplayName() {
//		return displayName;
//	}
//
//	public void setDisplayName(String displayName) {
//		this.displayName = displayName;
//	}
//
//	public String getDescription() {
//		return description;
//	}
//
//	public void setDescription(String description) {
//		this.description = description;
//	}
//	
//	public boolean isTrusted() {
//		return isTrusted;
//	}
//
//	public void setTrusted(boolean isTrusted) {
//		this.isTrusted = isTrusted;
//	}
//
//	public Configurator getConfigurator() {
//		return configurator;
//	}
//
//	public void setConfigurator(Configurator configurator) {
//		this.configurator = configurator;
//	}
//
//	public AggregationProfile getProfile() {
//		return profile;
//	}
//
//	public void setProfile(AggregationProfile profile) {
//		this.profile = profile;
//	}
//    
    
    
}
