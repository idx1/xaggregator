package org.idx.aggregator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "APP_TYPE_CONFIG_PARAMS")
@Data
public class AppTypeParams {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int id;

	@Column(nullable=false)
	private String name;

	private String description;

	@Column(nullable=false)
	private APP_TYPE_PARAM_TYPE type;

	private String value;

	private String label;
	
	private boolean editable;	

	public enum APP_TYPE_PARAM_TYPE{
		STRING,NUMBER,BOOLEAN,DATE,SECRET;
	}
}
